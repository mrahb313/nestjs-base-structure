# NestJs Base Structure

#### This project includes base features of Nestjs for start developing your project

### Usage

- Development

```
STAGE=dev nest start --watch
yarn start:dev
```

- Production

```
STAGE=prod nest start --watch
yarn start:prod
```

#### Run By Docker
- Tips:

1. by default in docker-compose.yml file i've set Stage=dev so if you want
   to change it you can go into docker-compose.yml then change STAGE in
   Environment section of "api" service.

2. in .env.stage.dev file i've set DB_HOST to "host.docker.internal" because
   i've installed postgres db on my local machine and not as a docker service
   so you can change it to "localhost" or any other things which is correct for you.

```
docker-compose up -d
```