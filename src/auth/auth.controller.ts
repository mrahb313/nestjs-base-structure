import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';

@Controller('auth')
@ApiTags('Authentication')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  SignUp(@Body() authCredentialsDto: AuthCredentialsDto): Promise<void> {
    return this.authService.SignUp(authCredentialsDto);
  }

  @Post('/signin')
  SignIn(
    @Body() AuthCredentialsDto: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    return this.authService.SignIn(AuthCredentialsDto);
  }
}
