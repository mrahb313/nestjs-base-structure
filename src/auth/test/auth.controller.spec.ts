import { TestingModule, Test } from '@nestjs/testing';
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import { mockCreateUserDto, mockToken } from './auth-mock';

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const mockService = {
      SignUp: jest.fn().mockReturnValue(201),
      SignIn: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: mockService,
        },
      ],
    }).compile();
    //
    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('Should be defined', () => {
    expect(authController).toBeDefined();
  });

  describe('SignUp', () => {
    it('should call Signup and Create if everything be ok', async () => {
      const createSpy = jest.spyOn(authService, 'SignUp');

      const mockParam = mockCreateUserDto();
      await authController.SignUp(mockParam);

      expect(createSpy).toHaveBeenCalledWith(mockParam);
    });
  });

  describe('SignIn', () => {
    it('should call SignIn and get token if be ok', async () => {
      const returnValue = mockToken();

      const signinSpy = jest
        .spyOn(authService, 'SignIn')
        .mockResolvedValueOnce(returnValue);

      const mockParam = mockCreateUserDto();
      const res = await authController.SignIn(mockParam);

      expect(signinSpy).toHaveBeenCalledWith(mockParam);
      expect(res).toMatchObject({ accessToken: 'jwttoken' });
    });
  });
});
