import * as uuid from 'uuid';
import { AuthCredentialsDto } from '../dto/auth-credentials.dto';
import { User } from '../entity/user.entity';

export const mockCreateUserDto = (): AuthCredentialsDto => ({
  email: 'test@gmail.com',
  password: '#NestJs2021',
});

export const mockUser = (): User => ({
  id: uuid.v4(),
  email: 'test@gmail.com',
  password: '#NestJs2021',
});

export const mockToken = (): { accessToken: string } => ({
  accessToken: 'jwttoken',
});
